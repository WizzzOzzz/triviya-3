<%-- 
    Document   : play
    Created on : Feb 10, 2016, 1:03:40 AM
    Author     : WIZZZ-GAMEING
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.Random"%>
<%@page import="Triviya.Servlet.GameLoader"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Triviya.code.MultipleOptions"%>
<%@page import="Triviya.code.OpenQuestion"%>
<%@page import="Triviya.code.YesOrNo"%>
<%@page import="Triviya.code.Admin"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Triviya.code.SubjectsHolder"%>
<%@page import="Triviya.code.Question"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/Triviya3/CssStyle/Done.css">
        <title>JSP Page</title>
    </head>
    <body>
        <%!
            HttpSession session;
            ArrayList<Question> questions;
            Iterator<Question> iterator;
            int questionNumber;
            int score;
            String method;
            Question question = null;
            String yes = "Yes Or No";
            String mul = "Multiple Options";
            String op = "Open Question";
            Random random;
            HashMap<String, Integer[]> subjectDetails;
            int ran;
        %>
        <%
            subjectDetails = (HashMap) session.getAttribute("subjectDetails");
            method = "action=\"/Triviya3/increment\" method=\"post\"";
            random = new Random();
            session = request.getSession();
            questions = (ArrayList<Question>) session.getAttribute("questions");
            questionNumber = Integer.parseInt(session.getAttribute("questionNumber").toString());
            score = Integer.parseInt(session.getAttribute("score").toString());
            iterator = questions.iterator();
            if (questions.isEmpty()) {
        %><h2>There Are No Question In The Scope You Chose</h2>
        <a href="JSP/showScore.jsp"><button>Done</button></a><%
            session.setAttribute("questionNumber", 0);
        } else {
            for (int i = 0; i < questionNumber && iterator.hasNext(); i++) {
                question = iterator.next();
                if (i == questionNumber - 1) {
                    if (subjectDetails.containsKey(question.getSub())) {
                        Integer[] integers = subjectDetails.get(question.getSub());
                        integers[0]++;
                        subjectDetails.replace(question.getSub(), integers);
                    } else {
                        Integer[] integers = {1, 0};
                        subjectDetails.put(question.getSub(), integers);
                    }
                }
                if (i == questionNumber - 2 && request.getParameter("ans") != null && request.getParameter("ans").equals(question.getAns())) {
                    request.removeAttribute("ans");
                    score++;
                    Integer[] integers = subjectDetails.get(question.getSub());
                    integers[1]++;
                    subjectDetails.replace("subjectDetails", integers);
                }
                if (!iterator.hasNext() || i == 10) {
                    method = "action=\"/Triviya3/end\" method=\"post\"";
                }
            }
            if (session.getAttribute("first") != null && session.getAttribute("last") != null){
                %><h4>Player: <%=session.getAttribute("first").toString()%> <%=session.getAttribute("last").toString()%></h4><%
            }
            %><h4>Your Score So far: <%=score%></h4>
            <h4>Question Number: <%=questionNumber%></h4><%
                if (question.getBody().contains("?")) {
            %><%=question.getBody()%><%
            } else {
            %><%=question.getBody()%>?<%
                }
            %><form <%=method%>><%

                if (question.getType().equals(yes)) {
                %><input type="submit" name="ans" value="yes"><br>
                <input type="submit" name="ans" value="no"><br><%
                } else if (question.getType().equals(mul)) {
                    ran = random.nextInt(4);
                    int c = 0;
                    for (int i = 0; i < 4; i++) {
                        if (i == ran) {
                %><input type="radio" name="ans" value="<%=question.getAns()%>"><%=question.getAns()%><br><%
                } else {
                %><input type="radio" name="ans" value="<%=question.getFalseAns()[c]%>"><%=question.getFalseAns()[c]%><br><%
                            c++;
                        }
                    }
                %><input type="submit" value="Submit"><%
                } else if (question.getType().equals(op)) {
                %><input type="text" placeholder="Your Answer" name="ans"  autofocus required>
                <input type="submit" value="Submit"><%
                    }
                %></form><%
                        session.setAttribute("score", score);
                    }
                %>
            <form action="/Triviya3/end" method="post">
                <input id="logout" type="submit" name="done" value="Done">
            </form>
    </body>
</html>
