<%-- 
    Document   : addQuestion
    Created on : Jan 6, 2016, 4:53:24 PM
    Author     : oriwi_000
--%>

<%@page import="Triviya.code.QuestionHolder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/Triviya3/CssStyle/TriviyaCSS.css">
        <title>Add Question</title>
    </head>
    
        <%! HttpSession session;
            String method;
            String ansYes;
            String ansNo;
            String yesOrNo = "";
            String open = "";
            String multi = "";
            String formMethod;
            String submit;
            String disabled;
            String easy;
            String medium;
            String hard;
        %>
        <%
            easy="";
            medium="";
            hard="";
            session = request.getSession();%>
        <jsp:useBean id="question" class="Triviya.code.QuestionHolder" scope="session"/>
        <%if (request.getParameter("type") != null) {%>
        <jsp:setProperty name="question" property="type" value="<%=request.getParameter("type")%>" />
        <%}
            if (request.getParameter("sub") != null) {%>
        <jsp:setProperty name="question" property="sub" value="<%=request.getParameter("sub")%>" />
        <%}
            if (request.getParameter("body") != null) {%>
        <jsp:setProperty name="question" property="body" value="<%=request.getParameter("body")%>"/>
        <%}
            if (request.getParameter("ans") != null) {%>
        <jsp:setProperty name="question" property="ans" value="<%=request.getParameter("ans")%>"/>
        <%}
            if (request.getParameter("false0") != null) {%>
        <jsp:setProperty name="question" property="false0" value="<%=request.getParameter("false0")%>"/>
        <%}
            if (request.getParameter("false1") != null) {%>
        <jsp:setProperty name="question" property="false1" value="<%=request.getParameter("false1")%>"/>
        <%}
            if (request.getParameter("false2") != null) {%>
        <jsp:setProperty name="question" property="false2" value="<%=request.getParameter("false2")%>"/>
        <%}
            if (request.getParameter("ansYes") != null) {%>
        <jsp:setProperty name="question" property="ansYes" value="<%=request.getParameter("ansYes")%>"/>
        <%}
            if (request.getParameter("ansNo") != null) {%>
        <jsp:setProperty name="question" property="ansNo" value="<%=request.getParameter("ansNo")%>"/>
        <%}
            if (request.getParameter("yesOrNo") != null) {%>
        <jsp:setProperty name="question" property="yesOrNo" value="<%=request.getParameter("yesOrNo")%>"/>
        <%}
            if (request.getParameter("multi") != null) {%>
        <jsp:setProperty name="question" property="multi" value="<%=request.getParameter("multi")%>"/>
        <%}
            if (request.getParameter("open") != null) {%>
        <jsp:setProperty name="question" property="open" value="<%=request.getParameter("open")%>"/>
        <%}
            if (request.getParameter("disabled") != null) {%>
        <jsp:setProperty name="question" property="disabled" value="<%=request.getParameter("disabled")%>"/>
        <%}
            if (request.getParameter("yon") != null) {%>
        <jsp:setProperty name="question" property="yon" value="<%=request.getParameter("yon")%>"/>
        <%}
            if (request.getParameter("dif") != null) {%>
        <jsp:setProperty name="question" property="dif" value="<%=request.getParameter("dif")%>"/>
        <%}%>

        <%submit = "Continue";
            yesOrNo = "";
            open = "";
            multi = "";
            ansYes = "";
            ansNo = "";
            formMethod = "";
            if (request.getParameter("yon") != null) {
                if (!question.getYon().equals("no")) {
                    ansYes = "checked";
                } else {
                    ansNo = "checked";
                }
            }
            if (request.getParameter("type") != null) {
                if ((question.getType()).equals("Yes or no")) {
                    yesOrNo = "checked";
                } else if ((question.getType()).equals("Open question")) {
                    open = "checked";
                } else if ((question.getType()).equals("Multiple answers")) {
                    multi = "checked";
                }
            }

            if (request.getParameter("dif") != null) {
                if (question.getDif().equals("1")) {
                    easy = "checked";
                }
                if (question.getDif().equals("2")) {
                    medium = "checked";
                }
                if (question.getDif().equals("3")) {
                    hard = "checked";
                }
                formMethod = "action=\"Adder\" method=\"get\"";
            }
            if (request.getParameter("dif")!=null)
                    disabled = "disabled";
        %>
        <%--choosing question type--%>
        <h3>Please choose Question type:</h3><br>
        <form <%=formMethod%>>
            <%formMethod = "";%>
            <input type="radio" name="type" value="Yes or no" <%=yesOrNo%> <%=disabled%>> Yes Or No
            <input type="radio" name="type" value="Open question" <%=open%> <%=disabled%>> Open Question
            <input type="radio" name="type" value="Multiple answers" <%=multi%> <%=disabled%>> Multiple Answers
            <%if (request.getParameter("type") != null) {
            %><h3>Please choose Question Subject:</h3><br>
            <input type="text" placeholder="Question Subject" name="sub" value="<%=question.getSub()%>" autofocus required <%=disabled%>><br><%
                if (request.getParameter("sub") != null) {
                    //filling question body
            %>
            <h3>Please choose Question body:<br></h3>
            <input type="text" placeholder="Question Body" name="body" value="<%=question.getBody()%>" autofocus required <%=disabled%>><br><%
                if (request.getParameter("body") != null) {
                    if (!(question.getType().equals("Yes or no"))) {
                        //in case of NOT yes or no question
            %><h3>Please choose the question answer :</h3><br>
            <input type="text" placeholder="Question Answer" name="ans" value="<%=question.getAns()%>" autofocus required <%=disabled%>><br><%
                if (question.getType().equals("Multiple answers") && request.getParameter("ans") != null) {
                    //filling multiple answers type false answers
            %><h3>You need to choose 3 false answers :</h3><br>
            <h3>Please choose the first false answer:</h3><br>
            <input type="text" placeholder="Question False Answer" name="false0" value="<%=question.getFalse0()%>" autofocus required <%=disabled%>><br><%
                if (request.getParameter("false0") != null) {
            %><h3>Please choose the second false answer:</h3><br>
            <input type="text" placeholder="Question False Answer" name="false1" value="<%=question.getFalse1()%>" autofocus required <%=disabled%>><br><%
                if (request.getParameter("false1") != null) {
            %><h3>Please choose the third false answer:</h3><br>
            <input type="text" placeholder="Question False Answer" name="false2" value="<%=question.getFalse2()%>" autofocus required <%=disabled%>><br><%
                        }
                    }
                }
            } else {
                //in case of yes or no question
            %><h1>Please select the question answer :</h1><br>
            <input type="radio" name="yon" value="yes" <%=disabled%> <%=ansYes%> >Yes
            <input type="radio" name="yon" value="no" <%=disabled%> <%=ansNo%> >No<%
                    }
                }
                if (request.getParameter("false2") != null
                        || request.getParameter("yon") != null
                        || (request.getParameter("ans") != null && question.getType().equals("Open question"))) {
            %><h1>Please set question difficulty :</h1><br>
            <input type="radio" name="dif" value="1" <%=easy%> <%=disabled%>> Easy
            <input type="radio" name="dif" value="2" <%=medium%> <%=disabled%>>Medium
            <input type="radio" name="dif" value="3" <%=hard%> <%=disabled%>>Hard<%
                        }
                    }
                }

                if (request.getParameter("dif") != null) {
                    submit = "Finish";
                    disabled = "";
                }%> 
            <input id="continue" type="submit" value="<%=submit%>">
        </form>
        <%if (request.getSession().getAttribute("first") != null && request.getSession().getAttribute("last") != null) {
        %><form action="/Triviya3/logout" method="get">
            <input id="logout" type="submit" name="logout" value="Log Out">
        </form><%
            }
        %>
    

