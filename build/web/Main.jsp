<%-- 
    Document   : Main
    Created on : Jan 16, 2016, 1:49:07 PM
    Author     : oriwi_000
--%>

<%@page import="Triviya.code.Logout"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>VogWiz</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="CssStyle/TriviyaCSS.css">
        <link rel="stylesheet" type="text/css" href="CssStyle/SideBarCSS.css">
    </head>
    <body>
        <div id="container">
            <div id="header">
                <a href="/Triviya3"><img src="Images/Background.jpeg" alt="Logo" width="250" height="140"></a>
            </div>
            <form action="go" method="GET">
                <div id="SideBar">
                    <br><br>
                    <ul>
                        <li><a href="about.jsp" target="dis"><br><br>About<br><br><br></a>
                        <li><a href="similarWebGames.jsp" target="dis"><br><br>Similar web games<br><br><br></a>
                        <li><a href="init" target="dis"><br><br>Start the game<br><br><br></a>
                    </ul>
                </div>
            </form>
            <div>
                <iframe name="dis" src="about.html" id="center" scrolling="horizinal">
                </iframe>
            </div>
            <div id="footer">
                Copyright © VogWizzz
            </div>
        </div>
    </body>
</html>