<%-- 
    Document   : options
    Created on : Jan 6, 2016, 8:41:38 AM
    Author     : oriwi_000
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.io.IOException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/Triviya3/CssStyle/TriviyaCSS.css">
        <title>Admin Options</title>
    </head>
    <body>
        <div>
            <%!String fname, lname;%>
            <%session = request.getSession();
                if (session.getAttribute("new") != null && session.getAttribute("new").equals("true")) {
            %><h3>Hey <%=session.getAttribute("first").toString()%> <%=session.getAttribute("last")%>,  </h3><%
            } else {
                    session.setAttribute("new", "true");
                try {
            %><jsp:useBean id="question" type="Triviya.code.QuestionHolder" scope="session"/><%
                if (Triviya.code.Admin.createQuestion(question)) {
                    question.reset();
            %><h3> Question has been added </h3><%
            } else {
            %><h3>The Question Is Already Exist Or failed To Be Added</h3><%
                        }
                    } catch (Exception e) {
                        %><h3>There Has Been An Error Adding Your Question!,  </h3><%
                    }
                    fname = session.getAttribute("first").toString();
                    lname = session.getAttribute("last").toString();
                    session.invalidate();
                    session = request.getSession();
                    session.setAttribute("new", "true");
                    session.setAttribute("first", fname);
                    session.setAttribute("last", lname);
                }

                //remove all of the attributes of the request
                session.removeAttribute("question");
                request.removeAttribute("type");
                request.removeAttribute("sub");
                request.removeAttribute("body");
                request.removeAttribute("ans");
                request.removeAttribute("false0");
                request.removeAttribute("false1");
                request.removeAttribute("false2");
                request.removeAttribute("yon");
                request.removeAttribute("dif");
                request.removeAttribute("easy");
                request.removeAttribute("medium");
                request.removeAttribute("hard");

                //DONE
            %><h3> What would like to do? </h3><br>
            <form action="/Triviya3/forworder" method="GET">
                <input type="submit" name="add" value="Add"><br><br>
            </form>
            <form action="/Triviya3/JSP/deleteQuestion.jsp" method="GET">
                <input type="submit" name="delete" value="Delete"><br><br>
            </form>
            <form action="/Triviya3/JSP/GameConfig.jsp" method="GET">
                <input type="submit" name="test" value="Test The Game"><br><br>
            </form>
        </div>
        <%if (request.getSession().getAttribute("first") != null && request.getSession().getAttribute("last") != null) {
        %><form action="/Triviya3/logout" method="get">
            <input id="logout" type="submit" name="logout" value="Log Out">
        </form><%
            }
        %>
    </body>
</html>