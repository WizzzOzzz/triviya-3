<%-- 
    Document   : login
    Created on : Jan 6, 2016, 8:23:27 AM
    Author     : oriwi_000
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%! String first = "";
        String last = "";
        Cookie[] cookies;
        %>
        <%
            session = request.getSession();
            if (session.getAttribute("first") == null || session.getAttribute("last") == null) {
                cookies = request.getCookies();
                for (Cookie cookie : cookies){
                    if (cookie.getName().equals("first")){
                        first = "value=\"" +cookie.getValue() + "\"";
                    }
                    if (cookie.getName().equals("last")){
                        last = "value=\"" +cookie.getValue() + "\"";
                    }
                }
        %>
        <h1>Welcome Dear Admin</h1>
        <h2>Please login:</h2>
        <div>
            <form action="/Triviya3/auth" method="post">
                First Name: <input type="text" placeholder="First Name" <%=first%> name="first" autofocus required><br>
                Last Name: <input type="text" placeholder="Last Name" <%=last%> name="last" autofocus required><br>
                <input type="radio" name="remember">Remember Me<br>
                <input type="submit" value="Submit">
            </form>
        </div>
        <%} else {
                RequestDispatcher dispatcher = request.getRequestDispatcher("options.jsp");
                dispatcher.forward(request, response);
            }
        %>
    </body>
</html>
