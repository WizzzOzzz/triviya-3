<%-- 
    Document   : GameConfig
    Created on : Feb 9, 2016, 3:13:33 PM
    Author     : WIZZZ-GAMEING
--%>

<%@page import="Triviya.code.Admin"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" href="/Triviya3/CssStyle/TriviyaCSS.css" rel="stylesheet">
        <title>Game Configuration</title>
    </head>
    <body>
        <jsp:useBean id="subjectsHolder" class="Triviya.code.SubjectsHolder" scope="session"/>
        <h2>Choose The Subjects For The Game:</h2><br>
        <%!ResultSet resultSetSub;
            ResultSet resultSetQuestions;
            PreparedStatement preparedStatement;
            Connection connection;
            String originSub;
            String sub;
            String disabled = "";
            String choose = "Choose";
            String checked;
            String method = "";
            ArrayList<String> subjects;
            HttpSession session;
            int counter;%>
        <%
            connection = Admin.connectToDB();
            preparedStatement = connection.prepareStatement("select * from subjects");
            resultSetSub = preparedStatement.executeQuery();
            subjects = new ArrayList<String>();
            counter = 0;
            while (resultSetSub.next()) {
                sub = resultSetSub.getString(1);
                originSub = sub.replaceAll("_", " ");
                subjects.add(sub);
            }
        %><jsp:setProperty name="subjectsHolder" property="subjects" value="<%=subjects%>"/><%
        %><form action="/Triviya3/load" method="post"><%
            for (String subject : subjects) {
                originSub = subject.replaceAll("_", " ");
            %><input type="checkbox" name="<%=subject%>" value="on"> <%=originSub%><br><%
                }
            %><h2>Please Choose The Maximum Difficulty</h2>
            <input type="radio" name="dif" value="easy" >Easy
            <input type="radio" name="dif" value="medium"  >Medium
            <input type="radio" name="dif" value="hard"  >Hard
            <input type="submit" name="start" value="Begin"><%
                session.setAttribute("questionNumber", 1);
                session.setAttribute("score", 0);
            %></form>
            <%if (request.getSession().getAttribute("first") != null && request.getSession().getAttribute("last") != null) {
            %><form action="/Triviya3/logout" method="get">
            <input id="logout" type="submit" name="logout" value="Log Out">
        </form><%
            }
        %>
    </body>
</html>
