<%-- 
    Document   : showScore
    Created on : Feb 10, 2016, 3:00:49 AM
    Author     : WIZZZ-GAMEING
--%>

<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Triviya.code.SubjectsHolder"%>
<%@page import="Triviya.code.QuestionHolder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%!String fname = null;
            String lname = null;
            HttpSession session;
            String fullName = "";
            HashMap<String, Integer[]> subjectDetails;
            Set<String> subjects;
        %>
        <%
            subjectDetails = (HashMap) session.getAttribute("subjectDetails");
            session = request.getSession();
            request.removeAttribute("dif");
            if (session.getAttribute("subjectsHolder") != null) {
                ArrayList<String> subjects = ((SubjectsHolder) session.getAttribute("subjectsHolder")).getSubjects();
                for (String subject : subjects) {
                    request.removeAttribute(subject);
                }
            }
            if (session.getAttribute("first") != null && session.getAttribute("last") != null) {
                fullName = " " + session.getAttribute("first").toString() + " " + session.getAttribute("last").toString();
            }
        %><h3>congratulations<%=fullName%>, You Finish!!!!!</h3>
        Your Score Is : <%=session.getAttribute("score")%> Out Of <%=session.getAttribute("questionNumber")%>!<br>
        <%
            if (session.getAttribute("first") != null && session.getAttribute("last") != null) {
                fname = session.getAttribute("first").toString();
                lname = session.getAttribute("last").toString();
            }
            subjects = subjectDetails.keySet();
            for (String subject : subjects) {
                %><h3>for <%=subject%>: You have been questioned <%=subjectDetails.get(subject)[0]%> questions, you answered correctly on <%=subjectDetails.get(subject)[1]%> of them.</h3><%
            }
            request.removeAttribute("dif");
            session.invalidate();
            session = request.getSession();
            session.setAttribute("new", "true");
            if (fname != null || lname != null) {
                session.setAttribute("first", fname);
                session.setAttribute("last", lname);
            }
        %>
    </body>
</html>
