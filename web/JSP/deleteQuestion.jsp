<%-- 
    Document   : deleteQuestion
    Created on : Jan 6, 2016, 4:54:46 PM
    Author     : oriwi_000
--%>

<%@page import="java.sql.Connection"%>
<%@page import="Triviya.code.Admin"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.sun.xml.ws.tx.at.v10.types.PrepareResponse"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" href="/Triviya3/CssStyle/TriviyaCSS.css" rel="stylesheet">
        <title>Delete Question</title>
    </head>
    <body>
        <%!ResultSet resultSetSub;
            ResultSet resultSetQuestions;
            PreparedStatement preparedStatement;
            Connection connection;
            String originSub;
            String sub;
            int selected;
        %>
        <%connection = Admin.connectToDB();
            preparedStatement = connection.prepareStatement("select * from subjects");
            resultSetSub = preparedStatement.executeQuery();
            if (!resultSetSub.next()) {
        %><h3>There Are No Questions In The Database</h3>
        <a href="/Triviya3/JSP/options.jsp"><button id="">Back</button></a><%
        } else {
        %><h2>Mark the question you want to delete</h2><%
            selected = 1;
            preparedStatement = connection.prepareStatement("select * from subjects");
            resultSetSub = preparedStatement.executeQuery();
            while (resultSetSub.next()) {
                sub = resultSetSub.getString(1);
                originSub = sub.replaceAll("_", " ");
        %><h3>For The Subject <%=originSub%>:</h3><%
            preparedStatement = connection.prepareStatement("select body from questions where subject=?");
            preparedStatement.setString(1, sub);
            resultSetQuestions = preparedStatement.executeQuery();
        %><form action="/Triviya3/delete" method="post"><%
            while (resultSetQuestions.next()) {
            %><input type="radio" name="forDelete" value="<%=selected%>"> <%=selected%> <%=resultSetQuestions.getString("body")%><br><%
                        selected++;
                    }
                }
            %><input type="submit" name="delete" value="Delete"><br>
        </form><%
            }
        %>
        <%if (request.getSession().getAttribute("first") != null && request.getSession().getAttribute("last") != null) {
        %><form action="/Triviya3/logout" method="get">
            <input id="logout" type="submit" name="logout" value="Log Out">
        </form><%
            }
        %>
    </body>
</html>
