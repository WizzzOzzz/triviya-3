<%-- 
    Document   : similarWebGames
    Created on : Mar 3, 2016, 1:49:57 PM
    Author     : oriwi_000
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>VogWiz</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="/Triviya3/CssStyle/TriviyaCSS.css">
    </head>
    <body>
        <div id="center">
            <h1>These are similar games that we've found on the Internet</h1>
            <a href="http://www.netrivia.co.il/" target="http://www.netrivia.co.il/" style="font-size:22px">Netrivia</a><br>
            <a href="http://www.mytrivia.co.il/" target="http://www.mytrivia.co.il/" style="font-size:22px">My Trivia</a><br>
            <a href="http://www.yo-yoo.co.il/trivia/" target="http://www.yo-yoo.co.il/trivia/" style="font-size:22px">Yoyo Trivia</a><br>
        </div>
        <%if (request.getSession().getAttribute("first") != null && request.getSession().getAttribute("last") != null) {
        %><form action="/Triviya3/logout" method="get">
            <input id="logout" type="submit" name="logout" value="Log Out">
        </form><%
            }
        %>
    </body>
</html>
