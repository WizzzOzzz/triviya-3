/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author WIZZZ-GAMEING
 */
public class QuitGame extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.removeAttribute("question");
        request.removeAttribute("questionNumber");
        request.removeAttribute("score");
        request.removeAttribute("subjects");
        RequestDispatcher dispatcher;
        if (request.getSession().getAttribute("first") != null && request.getSession().getAttribute("last") != null){
        dispatcher = request.getRequestDispatcher("/Triviya3/JSP/options.jsp");
        }else{
        dispatcher = request.getRequestDispatcher("/Triviya3/JSP/login.jsp");
        }
    }
}
