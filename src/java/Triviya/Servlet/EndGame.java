/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.Servlet;

import Triviya.code.Question;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author WIZZZ-GAMEING
 */
public class EndGame extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        int questionNumber;
        int score;
        Iterator<Question> iterator;
        ArrayList<Question> questions;
        Question question;
        String fname;
        String lname;
        HashMap<String, Integer[]> subjectDetails = (HashMap) session.getAttribute("subjectDetails");
        questions = (ArrayList<Question>) session.getAttribute("questions");
        iterator = questions.iterator();

        questionNumber = Integer.parseInt(session.getAttribute("questionNumber").toString());
        if (request.getParameter("done") != null) {
            session.setAttribute("questionNumber", questionNumber - 1);
            for (int i = 0; i < questionNumber && iterator.hasNext(); i++) {
                question = iterator.next();
                if (i == questionNumber - 1) {
                    Integer[] integers = subjectDetails.get(question.getSub());
                    if (integers[0] == 1) {
                        subjectDetails.remove(question.getSub());
                    } else {
                        integers[0]--;
                        subjectDetails.replace(question.getSub(), integers);
                    }
                }
            }
        } else {
            score = Integer.parseInt(session.getAttribute("score").toString());
            for (int i = 0; i < questionNumber && iterator.hasNext(); i++) {
                question = iterator.next();
                if (i == questionNumber - 1 && request.getParameter("ans") != null && request.getParameter("ans").equals(question.getAns())) {
                    score++;
                    Integer[] integers = subjectDetails.get(question.getSub());
                    integers[1]++;
                    subjectDetails.replace("subjectDetails", integers);
                }

            }
            session.setAttribute("score", score);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("JSP/showScore.jsp");
        dispatcher.forward(request, response);

    }
}
