/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.Servlet;

import Triviya.code.Admin;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author WIZZZ-GAMEING
 */
public class DeleteQuestion extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ResultSet resultSetSub;
        ResultSet resultSetQuestions = null;
        PreparedStatement preparedStatement;
        String sub = "";
        String type = "";
        boolean found = false;
        int selected = 1;
        Connection connection;
        try {
            connection = Admin.connectToDB();

            preparedStatement = connection.prepareStatement("select * from subjects");
            resultSetSub = preparedStatement.executeQuery();
            while (resultSetSub.next()) {
                sub = resultSetSub.getString(1);
                preparedStatement = connection.prepareStatement("select * from questions where subject=?");
                preparedStatement.setString(1, sub);
                resultSetQuestions = preparedStatement.executeQuery();
                while (resultSetQuestions.next()) {
                    if (selected == Integer.parseInt(request.getParameter("forDelete"))) {
                        found = true;
                        break;
                    }
                    selected++;
                }
                if (found) {
                    break;
                }
            }
            if (resultSetQuestions.getString("qtype").equals("Yes Or No")) {
                type = "yesorno";
            } else if (resultSetQuestions.getString("qtype").equals("Open Question")) {
                type = "openquestion";
            } else if (resultSetQuestions.getString("qtype").equals("Multiple Options")) {
                type = "multipleoptions";
            }
            preparedStatement = connection.prepareStatement("delete from questions where qid=?");
            preparedStatement.setInt(1, resultSetQuestions.getInt("qid"));
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("delete from " + type + " where qid=?");
            preparedStatement.setInt(1, resultSetQuestions.getInt("qid"));
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("delete from " + resultSetQuestions.getString("subject") + " where qid=?");
            preparedStatement.setInt(1, resultSetQuestions.getInt("qid"));
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("UPDATE subjects SET question_counter=? where subject=?");
            preparedStatement.setInt(1, resultSetSub.getInt("question_counter") - 1);
            preparedStatement.setString(2, resultSetQuestions.getString("subject"));
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement("select * from subjects where subject=?");
            preparedStatement.setString(1, sub);
            resultSetSub = preparedStatement.executeQuery();
            resultSetSub.next();
            if (resultSetSub.getInt("QUESTION_COUNTER") == 0) {
                preparedStatement = connection.prepareStatement("DROP TABLE " + resultSetQuestions.getString("subject"));
                preparedStatement.executeUpdate();
                preparedStatement = connection.prepareStatement("delete from subjects where subject=?");
                preparedStatement.setString(1, resultSetQuestions.getString("subject"));
                preparedStatement.executeUpdate();

            }
        } catch (SQLException ex) {
            Logger.getLogger(DeleteQuestion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DeleteQuestion.class.getName()).log(Level.SEVERE, null, ex);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("JSP/deleteQuestion.jsp");
        dispatcher.forward(request, response);
    }
}
