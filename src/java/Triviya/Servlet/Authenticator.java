/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.Servlet;

import Triviya.code.Admin;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author WIZZZ-GAMEING
 */
public class Authenticator extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection connection = null;
        try {
            connection = Admin.connectToDB();
            preparedStatement = connection.prepareStatement("select * from users");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                if (resultSet.getString("name").equals(request.getParameter("first").toLowerCase()) && resultSet.getString("family").equals(request.getParameter("last").toLowerCase())){
                    storeName(request, response);
                }
            }
        } catch (Exception e) {
            request.getRequestDispatcher("JSPErrors/SQLError.jsp").forward(request, response);
        }
        
    }
    
    private void storeName(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        if (request.getParameter("remember")!=null){
            Cookie first = new Cookie("first", request.getParameter("first"));
            Cookie last = new Cookie("last", request.getParameter("last"));
            first.setMaxAge(60*60*24*7);
            last.setMaxAge(60*60*24*7);
            response.addCookie(first);
            response.addCookie(last);
        }
        HttpSession session = request.getSession();
        session.setAttribute("first", request.getParameter("first"));
        session.setAttribute("last", request.getParameter("last"));
        request.removeAttribute("first");
        request.removeAttribute("last");
        request.getSession().setAttribute("new", "true");
        RequestDispatcher dispatcher = request.getRequestDispatcher("JSP/options.jsp");
        dispatcher.forward(request, response);
        
    }

}
