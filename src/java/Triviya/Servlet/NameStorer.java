/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author oriwi_000
 */
public class NameStorer extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("remember")!=null){
            Cookie first = new Cookie("first", request.getParameter("first"));
            Cookie last = new Cookie("last", request.getParameter("last"));
            response.addCookie(first);
            response.addCookie(last);
        }
        HttpSession session = request.getSession();
        session.setAttribute("first", request.getParameter("first"));
        session.setAttribute("last", request.getParameter("last"));
        request.removeAttribute("first");
        request.removeAttribute("last");
        request.getSession().setAttribute("new", "true");
        RequestDispatcher dispatcher = request.getRequestDispatcher("JSP/options.jsp");
        dispatcher.forward(request, response);
    }
}
