/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.Servlet;

import java.beans.Statement;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author oriwi_000
 */
public class InitializeEnviroment extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (initialEnviroment(request, response)) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("JSP/mainPageView.jsp");
            dispatcher.forward(request, response);
        }

    }

    public boolean initialEnviroment(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = null;
        boolean error = false;
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            String urlCn = "jdbc:derby://localhost:1527/Triviya";
            Connection cn = DriverManager.getConnection(urlCn, "vogwiz", "1234");
          //  java.sql.Statement st = cn.createStatement();
            //ResultSet rs = st.executeQuery("select * from users");
        } catch (ClassNotFoundException cNFE) {
            error = true;
            dispatcher = request.getRequestDispatcher("JSPErrors/ClassError.jsp");
        } catch (SQLException sqlE) {
            error = true;
            dispatcher = request.getRequestDispatcher("JSPErrors/SQLError.jsp");
        }
        if (error){
            dispatcher.forward(request, response);
            return false;
            }
        return true;
    }
}