/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.Servlet;

import Triviya.code.Admin;
import Triviya.code.MultipleOptions;
import Triviya.code.OpenQuestion;
import Triviya.code.Question;
import Triviya.code.SubjectsHolder.*;
import Triviya.code.SubjectsHolder;
import Triviya.code.YesOrNo;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sun.awt.SunToolkit;

/**
 *
 * @author WIZZZ-GAMEING
 */
public class GameLoader extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PreparedStatement preparedStatement;
        HttpSession session;
        ResultSet resultSetSub;
        Connection connection = null;
        ArrayList<Question> questions;
        SubjectsHolder subjectsHolder;
        ArrayList<String> subjects;
        Iterator<String> iterator;
        String[] falseAns;
        int questionNumber;
        int score;
        String subject;
        Random random = new Random();
        int max;
        ArrayList<Integer> suffleIndex = new ArrayList<>();
        Question[] suffleQuestions;
        HashMap<String, Integer[]> subjectDetails = new HashMap<>();

        session = request.getSession();
        String sqlQuery = "";
        int dif;
        questions = new ArrayList<Question>();
        boolean noSubjectSelected = true;
        session.setAttribute("dif", request.getParameter("dif"));
        for (String noSubject : ((SubjectsHolder) session.getAttribute("subjectsHolder")).getSubjects()) {
            if (request.getParameter(noSubject) != null) {
                noSubjectSelected = false;
                break;
            }
        }
        if (session.getAttribute("dif") == null || noSubjectSelected) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("JSPErrors/NoCategoryOrLevel.jsp");
            dispatcher.forward(request, response);
        } else {
            try {
                connection = Admin.connectToDB();
                subjectsHolder = (SubjectsHolder) session.getAttribute("subjectsHolder");
                subjects = subjectsHolder.getSubjects();
                iterator = subjects.iterator();
                sqlQuery = "select * from questions where (";
                if (session.getAttribute("dif").equals("easy")) {
                    dif = 1;
                } else if (session.getAttribute("dif").equals("medium")) {
                    dif = 2;
                } else {
                    dif = 3;
                }
                for (; dif > 0; dif--) {
                    if (dif == 3) {
                        sqlQuery += "dificulty='hard' or ";
                    } else if (dif == 2) {
                        sqlQuery += "dificulty='medium' or ";
                    } else {
                        sqlQuery += "dificulty='easy'";
                    }
                }
                sqlQuery += ") and (";
                while (iterator.hasNext()) {
                    subject = iterator.next();
                    if (request.getParameter(subject) != null) {
                        sqlQuery += "subject='" + subject + "'";
                        sqlQuery += " or ";
                    }
                }
                String forCheck = sqlQuery.substring(sqlQuery.length() - 3, sqlQuery.length() - 1);
                if (forCheck.equals("or")) {
                    StringBuffer stringBuffer = new StringBuffer(sqlQuery);
                    stringBuffer.deleteCharAt(sqlQuery.length() - 1);
                    stringBuffer.deleteCharAt(sqlQuery.length() - 2);
                    stringBuffer.deleteCharAt(sqlQuery.length() - 3);
                    stringBuffer.deleteCharAt(sqlQuery.length() - 4);
                    sqlQuery = stringBuffer.toString();
                }
                sqlQuery += ")";
                preparedStatement = connection.prepareStatement(sqlQuery);
                resultSetSub = preparedStatement.executeQuery();
                while (resultSetSub.next()) {
                    if (resultSetSub.getString("qtype").equals("Yes Or No")) {
                        questions.add(new YesOrNo(dif, resultSetSub.getString("subject"), resultSetSub.getString("body"), resultSetSub.getString("answer")));
                    } else if (resultSetSub.getString("qtype").equals("Open Question")) {
                        questions.add(new OpenQuestion(dif, resultSetSub.getString("subject"), resultSetSub.getString("body"), resultSetSub.getString("answer")));
                    } else {
                        falseAns = new String[3];
                        falseAns[0] = resultSetSub.getString("false1");
                        falseAns[1] = resultSetSub.getString("false2");
                        falseAns[2] = resultSetSub.getString("false3");
                        questions.add(new MultipleOptions(dif, resultSetSub.getString("subject"), resultSetSub.getString("body"), resultSetSub.getString("answer"), falseAns));
                    }

                }
                max = questions.size();
                suffleQuestions = new Question[max];
                for(Question question : questions){
                    int chosenIndex;
                    do {
                    chosenIndex = random.nextInt(max);
                    }while (suffleIndex.contains(chosenIndex));
                    suffleIndex.add(chosenIndex);
                    suffleQuestions[chosenIndex] = question;
                }
                questions.clear();
                for (Question question : suffleQuestions){
                    questions.add(question);
                }
                session.setAttribute("questions", questions);
                session.setAttribute("subjectDetails", subjectDetails);
                RequestDispatcher dispatcher = request.getRequestDispatcher("JSP/play.jsp");
                dispatcher.forward(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(GameLoader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(GameLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
