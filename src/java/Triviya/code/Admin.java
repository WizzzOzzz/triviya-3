/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

import static Triviya.code.DatabaseEnum.*;
import com.sun.corba.se.spi.presentation.rmi.StubAdapter;
import com.sun.xml.ws.tx.coord.common.PendingRequestManager;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 *
 * @author oriwi_000
 */
public class Admin extends User implements Serializable {

    private static Connection connection;

    private static Statement st;

    private static HashMap<String, ArrayList<Question>> hm;

    public static Connection connectToDB() throws SQLException, ClassNotFoundException {
        if (connection != null) {
            return connection;
        }
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        String urlCn = "jdbc:derby://localhost:1527/Triviya";
        return DriverManager.getConnection(urlCn, USER.toString(), PASS.toString());

    }

    public static void writeToDB(HashMap<String, ArrayList<Question>> hm) {

    }

    public static boolean readFromDB(Question q) throws ClassNotFoundException, SQLException {
        connection = connectToDB();
        st = connection.createStatement();

        String sub = q.getSub().trim().replaceAll(" ", "_");
        ResultSet resultSet = st.executeQuery("SELECT * FROM SUBJECTS");
        while (resultSet.next()) {
            if (resultSet.getString(1).equals(sub)) {
                return true;
            }
        }

        return false;
    }

    public static void deleteQuestion(Question q) throws ClassNotFoundException, SQLException {
        if (readFromDB(q)) {

        }
    }

    public static boolean addQuestion(Question q) throws SQLException, ClassNotFoundException {
        ResultSet rs;
        String sub = q.getSub().trim().replaceAll(" ", "_");
        String type = "";
        if (q.getType().equals("Yes Or No")) {
            type = "y";
        } else if (q.getType().equals("Open Question")) {
            type = "o";
        } else if (q.getType().equals("Multiple Options")) {
            type = "m";
        }
        PreparedStatement prepareStatement;
        ResultSet resultSet;
        connection = connectToDB();
        prepareStatement = connection.prepareStatement("select * from QUESTIONS where subject=? AND body=? AND qtype=?");
            prepareStatement.setString(1, sub);
            prepareStatement.setString(2, q.getBody());
            prepareStatement.setString(3, q.getType());
            resultSet = prepareStatement.executeQuery();
            if (resultSet.next()) {
                return false;
            }
        String dif = "";
        if (q.getDif() == 1) {
            dif = "easy";
        } else if (q.getDif() == 2) {
            dif = "medium";
        } else {
            dif = "hard";
        }
        String ans = q.getAns();
        if (type.equals("y")) {
            if (((YesOrNo) q).isPositiveAns()) {
                ans = "yes";
            } else {
                ans = "no";
            }
        }

        String[] falseAns = q.getFalseAns();
        String insertToTable;
        if (q.getType().equals("Multiple Options")) {
            prepareStatement = connection.prepareStatement("insert into questions (BODY,ANSWER,false1,false2,false3,SUBJECT,QTYPE,DIFICULTY)"
                    + "values( ?,?,?,?,?,?,?,?)");
            prepareStatement.setString(1, q.getBody());
            prepareStatement.setString(2, ans);
            prepareStatement.setString(3, falseAns[0]);
            prepareStatement.setString(4, falseAns[1]);
            prepareStatement.setString(5, falseAns[2]);
            prepareStatement.setString(6, sub);
            prepareStatement.setString(7, q.getType());
            prepareStatement.setString(8, dif);
            prepareStatement.executeUpdate();
        } else {
            prepareStatement = connection.prepareStatement("insert into questions (BODY,ANSWER,SUBJECT,QTYPE,DIFICULTY)"
                    + "values( ?,?,?,?,?)");
            prepareStatement.setString(1, q.getBody());
            prepareStatement.setString(2, ans);
            prepareStatement.setString(3, sub);
            prepareStatement.setString(4, q.getType());
            prepareStatement.setString(5, dif);
            prepareStatement.executeUpdate();
        }
        prepareStatement = connection.prepareStatement("select * from questions where body=? and subject=? and qtype=?");
        prepareStatement.setString(1, q.getBody());
        prepareStatement.setString(2, sub);
        prepareStatement.setString(3, q.getType());
        resultSet = prepareStatement.executeQuery();
        int qid = -1;
        if (resultSet.next()) {
            qid = resultSet.getInt("qid");
        }
        if (q.getType().equals("Yes Or No")) {
            type = "yesorno";
        } else if (q.getType().equals("Open Question")) {
            type = "openquestion";
        } else if (q.getType().equals("Multiple Options")) {
            type = "multipleoptions";
        }
        prepareStatement = connection.prepareStatement("insert into " + type + "(qid,subject,body,answer,dificulty)"
                + "values( ?,?,?,?,?)");
        prepareStatement.setInt(1, qid);
        prepareStatement.setString(2, sub);
        prepareStatement.setString(3, q.getBody());
        prepareStatement.setString(4, ans);
        prepareStatement.setString(5, dif);
        prepareStatement.executeUpdate();
        if (readFromDB(q)) {
            prepareStatement = connection.prepareStatement("select question_counter from VOGWIZ.subjects where subject=?");
            prepareStatement.setString(1, sub);
            rs = prepareStatement.executeQuery();
            rs.next();
            int newCounter = rs.getInt(1) + 1;
            prepareStatement = connection.prepareStatement("UPDATE subjects SET question_counter=? where subject=?");
            prepareStatement.setInt(1, newCounter);
            prepareStatement.setString(2, sub);
            prepareStatement.executeUpdate();
        } else {
            prepareStatement = connection.prepareStatement("insert into subjects values(?,?)");
            prepareStatement.setString(1, sub);
            prepareStatement.setInt(2, 1);
            prepareStatement.executeUpdate();
            String createTable = "CREATE TABLE " + sub
                    + " (qid integer not null, type varchar(18), body varchar(255), dificulty varchar(8)," + "primary key(qid))";
            st.executeUpdate(createTable);
        }
        prepareStatement = connection.prepareStatement("insert into " + sub + "(qid,type,body,dificulty)"
                + "values( ?,?,?,?)");
        prepareStatement.setInt(1, qid);
        prepareStatement.setString(2, q.getType());
        prepareStatement.setString(3, q.getBody());
        prepareStatement.setString(4, dif);
        prepareStatement.executeUpdate();
        return true;
    }

    public static boolean createQuestion(QuestionHolder q) throws IOException, ClassNotFoundException, SQLException {
        switch (q.getType()) {
            case "Yes or no":
                return yesOrNoAdd(q);
            case "Multiple answers":
                return MultipleOptionsAdd(q);
            case "Open question":
                return OpenQuestionAdd(q);
            default:
                return false;
        }
    }

    private static boolean yesOrNoAdd(QuestionHolder q) throws ClassNotFoundException, SQLException {
        boolean ans = true;
        if (q.getYon().equals("no")) {
            ans = false;
        }
        return addQuestion(new YesOrNo(Integer.parseInt(q.getDif()), q.getSub().toLowerCase(), q.getBody(), ans));
    }

    private static boolean MultipleOptionsAdd(QuestionHolder q) throws ClassNotFoundException, SQLException {
        String[] falseAns = {q.getFalse0(), q.getFalse1(), q.getFalse2()};
        return addQuestion(new MultipleOptions(Integer.parseInt(q.getDif()), q.getSub().toLowerCase(), q.getBody(), q.getAns(), falseAns));
    }

    private static boolean OpenQuestionAdd(QuestionHolder q) throws ClassNotFoundException, SQLException {
        return addQuestion(new OpenQuestion(Integer.parseInt(q.getDif()), q.getSub().toLowerCase(), q.getBody(), q.getAns()));
    }
}
