/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

import java.util.Scanner;

/**
 *
 * @author oriwi_000
 */
public class OpenQuestion extends Question {

    public OpenQuestion(int dif, String sub, String body, String ans) {
        super(dif);
        this.sub = sub;

        this.body = body;
        this.ans = ans;

        type = "Open Question";
    }


    public OpenQuestion getInfo(QuestionHolder q) {
        return new OpenQuestion( Integer.parseInt(q.getDif()), q.getSub(), q.getBody(), q.getAns());
    }

    public void openAns(Question q) {

        Scanner s = new Scanner(System.in);
        String ans;

        System.out.println("\nQuestion: " + q.body + "?");
        System.out.print("Answer: ");
        ans = s.nextLine();

        if (ans.equals(ans)) {
            System.out.println("\nCorrect answer!\n");
        } else {
            System.out.println("Wrong answer!");
        }

    }

}
