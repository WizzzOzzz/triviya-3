/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

/**
 *
 * @author oriwi_000
 */
public class User {

    ArrayList<Question> arr;
    ArrayList<Integer> difficulty = new ArrayList<Integer>();
    ArrayList<String> subject = new ArrayList<String>();

    String tmp;

    public void startGame() throws FileNotFoundException, IOException, ClassNotFoundException {
        System.out.println("The Session has began");
        HashMap<String, ArrayList<Question>> hm;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("Questions"));
            hm = (HashMap<String, ArrayList<Question>>) in.readObject();
            in.close();
        } catch (Exception c) {
            System.out.println("The file that contains the questions might be missing, try and add a question");
            return;
        }

        Iterator it = hm.entrySet().iterator();
        Scanner s = new Scanner(System.in);
        while (it.hasNext()) {

            Map.Entry<String, ArrayList<Question>> e = (Map.Entry<String, ArrayList<Question>>) it.next();

            System.out.println("please choose a category and difficulty from the following:\n");

            while (it.hasNext()) {
                e = (HashMap.Entry<String, ArrayList<Question>>) it.next();
                System.out.println("Category: " + e.getKey());
            }
            Boolean flag = true;
            System.out.println("");

            while (flag) {

                System.out.print("Category: ");
                s = new Scanner(System.in);
                subject.add(s.nextLine());
                System.out.println("");
                System.out.print("Difficulty: ");
                s = new Scanner(System.in);
                difficulty.add(s.nextInt());
                System.out.println("");

                s = new Scanner(System.in);
                System.out.println("\nWould you like to choose another category?");
                if (s.nextLine().equals("yes")) {
                    System.out.println("Select another category");
                } else {
                    flag = false;
                }

            }
            int i = -1;
            // it = hm.entrySet().iterator();
            for (String tmpsub : subject) {
                i++;
                arr = hm.get(tmpsub);

                for (Question q : arr) {
                    if (q.getDif() == difficulty.get(i)) {
                        if (q.getType().equals("YesOrNo")) {
                            YesOrNo y = (YesOrNo) q;
                            y.yesNoAnswer(q);
                        }

                        if (q.getType().equals("OpenQuestion")) {
                            OpenQuestion op = (OpenQuestion) q;
                            op.openAns(q);

                        }
                        if (q.getType().equals("MultipleOptions")) {
                            MultipleOptions mul = (MultipleOptions) q;
                            mul.multAns(mul);

                        }

                    }
                }
            }

        }
    }
}
