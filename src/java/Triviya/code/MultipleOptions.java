/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author oriwi_000
 */
public class MultipleOptions extends Question {

    private Random random = new Random();

    public MultipleOptions(int i) {
        super(i);
        type="Multiple Options";
    }
    
    public MultipleOptions(int dif, String sub, String body, String ans, String[] falseAns){
        super(sub, body, ans, falseAns, dif);
        type="Multiple Options";
    }
    
    public  MultipleOptions getInfo(QuestionHolder q){
        String[] falseAns = {q.getFalse0(),q.getFalse1(),q.getFalse2()};
        return new MultipleOptions(Integer.parseInt(q.getDif()),q.getSub(), q.getBody(), q.getAns(), falseAns);
    }

    /*public MultipleOptions(int dif,String sub, String body, String correctAns, String...wrongAns) {
        super(dif,sub,body);
        boolean sel = true;



        this.ans =correctAns;
        falseAns = new ArrayList();
        for (int i=0;i<wrongAns.length;i++)
            falseAns.add(wrongAns[i]);
        
        
        type = "Multiple Options";
    }*/
    
    public void multAns(Question q){
        String ans = "User Answer";
        int counter=1;       
        int number = 1+random.nextInt(falseAns.length);
        int correctAnsNum=number;
        
        System.out.println("\nQuestion: "+q.body+"?");        
        
        System.out.println("\nplease choose an answer number from following\n");        
        for (String wrongAns : falseAns) {           
            if(number == counter){                
                System.out.println("[ "+counter+" ]"+this.ans);
                counter++;                
            }
            System.out.println("[ "+counter+" ]"+wrongAns);
            counter++;
        }
  
    }
    
}
