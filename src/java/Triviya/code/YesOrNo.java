/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

import java.util.Scanner;

/**
 *
 * @author oriwi_000
 */
public class YesOrNo extends MultipleOptions {
    
    private boolean positiveAns;
    
    public YesOrNo(int dif, String sub, String body, String ans) {
        super(dif);
        this.sub = sub;
        this.body = body;
        this.ans = ans;
        type = "Yes Or No";
        /*
         sel=true;
        
         int d;
         String c;
         while (sel){
         System.out.println("Please Choose the question difficulty (0 - easy, 1 - medium, 2 - hard");
         c=s.nextLine();
         if (c.equals("cancel"))
         return;
         d = Integer.parseInt(c);
         dif=d;
         }
         */

    }
    public YesOrNo(int dif, String sub, String body, boolean ans){
        super(dif);
        this.sub=sub;
        this.body=body;
        if (positiveAns)
            this.ans = "yes";
        else
            this.ans = "no";
                    
        positiveAns = ans;
        type = "Yes Or No";
    }

    @Override
     public  YesOrNo getInfo(QuestionHolder q){
        return new YesOrNo(Integer.parseInt(q.getDif()), q.getSub(), q.getBody(), q.getAns().equals("yes"));
    }
     
    public boolean isPositiveAns() {
        return positiveAns;
    }

    public void setPositiveAns(boolean positiveAns) {
        this.positiveAns = positiveAns;
    }
     

    public String getAns() {
        return ans;
    }

    public void yesNoAnswer(Question q) {
 /*       System.out.println("Question: " + q.body + "?");
        System.out.println("[1] yes\n[2] no\n");
        s = new Scanner(System.in);

        ans = s.nextInt();
        if (ans == 1) {
            Ans = "yes";
        } else {
            Ans = "no";
        }

        if (ans == 0) {
            System.exit(1);
        }

        if (Ans.equals(q.correctAns)) {
            System.out.println("correct!\n");
        } else {
            System.out.println("wrong answer!\n\n");
        }
*/    }

}
