/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

import com.sun.org.apache.xalan.internal.xsltc.runtime.BasisLibrary;
import java.io.*;
import javax.persistence.Convert;

/**
 *
 * @author oriwi_000
 */
public abstract class Question implements Serializable {

    protected int dif; // 0 for easy 1 for med 2 for hard
    protected String sub; // question's subject
    protected String type;
    protected String body;
    protected String ans;
    protected String[] falseAns;
    
    public Question(int dif) {
        this.dif = dif;
    }

    public Question(int dif, String sub, String body) {
        this(dif);
        this.sub = sub;
        this.body = body;
    }
    
    public Question(String sub, String body, String ans, String[] falseAns, int dif ) {
        this(dif);
        this.sub = sub;
        this.body = body;
        this.type = type;
        this.ans = ans;
        this.falseAns = falseAns;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String questionType) {
        this.type = questionType;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String correctAns) {
        this.ans = correctAns;
    }

    public String[] getFalseAns() {
        return falseAns;
    }

    public void setFalseAns(String[] falseAns) {
        this.falseAns = falseAns;
    }

    public int getDif() {
        return dif;
    }
    
    public void setDif (int dif){
        this.dif = dif;
    }

    public String getSub() {
        return sub;
    }

    public boolean checkAns(String ans) {
        if (ans.equals("")) {
            return false;
        } // return correctAns.equals(ans);
        else {
            return true;
        }
    }
}
