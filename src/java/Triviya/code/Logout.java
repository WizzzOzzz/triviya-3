/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

/**
 *
 * @author oriwi_000
 */
public class Logout {

    private String logout;

    public Logout() {
        logout = "<%if (request.getSession().getAttribute(\"first\") != null && request.getSession().getAttribute(\"last\") != null) {\n"
                + "            %><form action=\"logout\" method=\"get\">\n"
                + "                <input type=\"submit\" value=\"Log Out\" id=\"logout\">\n"
                + "            </form><%\n"
                + "            }\n"
                + "            %>";
    }

    @Override
    public String toString() {
        return logout;
    }
}
