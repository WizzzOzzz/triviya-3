/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

/**
 *
 * @author oriwi_000
 */
public class QuestionHolder {

    private String type = "";
    private String sub = "";
    private String body = "";
    private String ans = "";
    private String false0 = "";
    private String false1 = "";
    private String false2 = "";
    private String method = "";
    private String ansYes = "";
    private String ansNo = "";
    private String yon = "";
    private String yesOrNo = "";
    private String open = "";
    private String multi = "";
    private String dif;
    private String disabled;
    
    public void reset(){
    type = "";
    sub = "";
    body = "";
    ans = "";
    false0 = "";
    false1 = "";
    false2 = "";
    method = "";
    ansYes = "";
    ansNo = "";
    yon = "";
    yesOrNo = "";
    open = "";
    multi = "";
    dif = "";
    disabled = "";
    }
    
    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public String getDif() {
        return dif;
    }

    public void setDif(String dif) {
        this.dif = dif;
    }
    
    public String getFalse2() {
        return false2;
    }

    public void setFalse2(String false2) {
        this.false2 = false2;
    }

    public String getYon() {
        return yon;
    }

    public void setYon(String yon) {
        this.yon = yon;
    }

    public QuestionHolder() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public String getFalse0() {
        return false0;
    }

    public void setFalse0(String false0) {
        this.false0 = false0;
    }

    public String getFalse1() {
        return false1;
    }

    public void setFalse1(String false1) {
        this.false1 = false1;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAnsYes() {
        return ansYes;
    }

    public void setAnsYes(String ansYes) {
        this.ansYes = ansYes;
    }

    public String getAnsNo() {
        return ansNo;
    }

    public void setAnsNo(String ansNo) {
        this.ansNo = ansNo;
    }

    public String getYesOrNo() {
        return yesOrNo;
    }

    public void setYesOrNo(String yesOrNo) {
        this.yesOrNo = yesOrNo;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getMulti() {
        return multi;
    }

    public void setMulti(String multi) {
        this.multi = multi;
    }
}
