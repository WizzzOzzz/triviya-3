/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya.code;

/**
 *
 * @author oriwi_000
 */
public enum DatabaseEnum {
    SELECT("select "),
    DELETE("delete "),
    OPEN_QUESTION("openquestions"),
    MULITPLE_OPTIONS("mulitpleoptions"),
    YES_OR_NO("yesornoquestion"),
    QUESTIONS("questions"),
    QUESTION_SUBJECT("questionsubject"),
    USER("vogwiz"),
    PASS("1234");
    
    private String showEvent;

    DatabaseEnum(String showEvent) {
        this.showEvent=showEvent;
    }
    
    public String toString(){
        return showEvent;
    }
    
    public String wrapWithBrackets(){
        return "{"+showEvent+"}";
    }
    
}
